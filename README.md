# Watch on Invidious
 
A simple script to redirect any video tab from youtube to the desired Invidious or Piped instance

forked from: https://greasyfork.org/en/scripts/478396-yt-invidious

## How to Install:

- Install a ***userscript manager***, like [ViolentMonkey](https://violentmonkey.github.io/).
- Open [this link](https://greasyfork.org/en/scripts/478964-watch-on-invidious-piped) and click Install, it will open a new tab; Click on install on this new tab.

### Firefox Users:
You will probably need to edit a config:

Go to `about:config`

search for `browser.tabs.loadDivertedInBackground` and change it to `true`

You might also need to install an [additional](https://greasyfork.org/en/scripts/447727-youtube-disable-inline-playback-keep-hovering-to-play) script if it isn't working, this will prevent thumbnail videos from loading, which can result in the script not working.

## How to use:
Do a mouse scroll, the border around the thumbnail will change to magenta and just middle click on the desired video, it will open a new background tab with the video on Invidious.

If you want to open on the same tab, you need to change `middleMouseRedirect` to `false`
